<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Track extends Model
{
    protected $fillable = [
    	'title',
    	'artist',
    	'file'
    ];

    public function getStatusAttribute($value){
        if($value == '1'){
            return true;
        }
        else{
            return false;
        }
    }

    public function getCreatedByAttribute($value){
        return (int)$value;
    }

    public function getCreatedAtAttribute($value){
        $time = strtotime($value);

        return Carbon::createFromTimestamp($time)->diffForHumans();
    }

    public function getUpdatedAtAttribute($value){
        $time = strtotime($value);

        return Carbon::createFromTimestamp($time)->diffForHumans();
    }


    public function artists(){
    	return $this->hasOne('App\Models\Artist', 'id', 'artist');
    } 

    public function audio(){
        return $this->hasOne('App\Models\File', 'id', 'file');
    }
}
