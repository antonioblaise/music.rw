<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Artist extends Model
{
    //
    protected $fillable = [
    	'name',
    	'profile_image',
    	'biography',
    	'status',
    	'created_by'
    ];

    public function getStatusAttribute($value){
    	if($value == '1'){
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public function getCreatedByAttribute($value){
    	return (int)$value;
    }

    public function getCreatedAtAttribute($value){
    	$time = strtotime($value);

    	return Carbon::createFromTimestamp($time)->diffForHumans();
    }

    public function getUpdatedAtAttribute($value){
    	$time = strtotime($value);

    	return Carbon::createFromTimestamp($time)->diffForHumans();
    }

    public function created_by(){
        return $this->belongsTo('App\Models\User','created_by', 'id');
    }



    public function createdBy(){
        return $this->belongsTo('App\Models\User','created_by', 'id');
    }

    public function image(){
        return $this->belongsTo('App\Models\File', 'profile_image', 'id');
    }

    public function profile_image(){
        return $this->belongsTo('App\Models\File', 'profile_image', 'id');
    }
}
