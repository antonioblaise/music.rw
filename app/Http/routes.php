<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['namespace' => 'Admin'], function(){
	Route::get('login', 'AdminController@login');
	Route::get('logout', 'AdminController@logout');
	Route::post('login', 'AdminController@postlogin');
	Route::post('upload', 'AdminController@upload');
});


// Admin Routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function(){
	// Resources
	Route::resource('artist', 'ArtistController');
	Route::resource('track', 'TrackController');
	Route::resource('video', 'VideoController');
	Route::resource('playlist', 'PlaylistController');
	Route::resource('user', 'UserController');

	Route::group(['prefix' => 'bulk'], function(){
		Route::get('track/add', function(){
			return ['page' => 'add bulk tracks'];
		});
		Route::get('video/add', function(){
			return ['page' => 'add bulk videos'];			
		});
	});

	Route::get('/', 'AdminController@home');
	Route::get('settings', 'AdminController@settings');
});


// Route for api
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function(){
	Route::any('/', function(){
		return [
			"api_version" => env('API_VERSION'),
			"app_version" => env('APP_VERSION'),
		];
	});
	Route::resource('artist', 'ArtistController');
	Route::resource('track', 'TrackController');
	Route::resource('video', 'VideoController');
	Route::resource('playlist', 'PlaylistController');
	Route::resource('user', 'UserController');
});