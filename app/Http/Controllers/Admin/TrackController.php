<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Track;
use App\Models\Artist;

use Auth;

use App\Http\Requests\StoreTrackRequest;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = ! $request->has('offline');
        $tracks = Track::where('status', $status)->with(['artists', 'audio'])->paginate(10);
        return view('admin.track')->withTracks($tracks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $artists = Artist::where('status', '1')->orderBy('name', 'ASC')->pluck('name', 'id');
        return view('admin.forms.track')->withTrack(null)->withArtists($artists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrackRequest $request)
    {
        $track = Track::create([
            'title' => $request->input('title'),
            'artist' => $request->input('artist'),
            'file' => $request->input('file'),
            'status' => $request->input('status'),
            'created_by' => $request->user()->id
        ]);

        if($track){
            return redirect('admin/track/'.$track->id.'/edit');
        }else{
            return back(); //with error ofcourse... lol
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $track = Track::with(['artists', 'audio'])->findOrFail($id);        
        $artists = Artist::where('status', '1')->orderBy('name', 'ASC')->pluck('name', 'id');
        return view('admin.forms.track')->withTrack($track)->withArtists($artists);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTrackRequest $request, $id)
    {
        $track = Track::findOrFail($id);
        $track->title = $request->input('title');
        $track->artist = $request->input('artist');
        $track->status = $request->input('status');
        $track->file = $request->input('file');

        $track->save();

        return redirect('admin/track/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $track = Track::findOrFail($id);
        $track->delete();
        return redirect('admin/track');
    }
}
