<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Artist;
use App\Http\Requests\StoreArtistRequest;

use Auth;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = ! $request->has('offline');
        $artists = Artist::with(['createdBy', 'image'])->where('status', $status)->orderBy('name', 'ASC')->paginate(12);
        return view('admin.artist')->withArtists($artists);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $artist = null;
        return view('admin.forms.artist')->withArtist($artist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArtistRequest $request)
    {
        $data  = $request->all();
        $data['created_by'] = Auth::user()->id;


        // validate and store data
        $artist = Artist::create($data);
        if($artist){
            return redirect('/admin/artist/'.$artist->id.'/edit');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $artist = Artist::findOrFail($id);
        return $artist;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artist = Artist::with('image')->findOrFail($id);
        return view('admin.forms.artist')->withArtist($artist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreArtistRequest $request, $id)
    {
        $artist = Artist::findOrFail($id);
        $artist->update($request->all());
        return redirect('/admin/artist/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artist = Artist::find($id);
        $artist->delete();
        return back();
    }
}
