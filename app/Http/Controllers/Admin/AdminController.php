<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\File;

use Auth;
use Carbon\Carbon;
use Image;

class AdminController extends Controller
{
    public function home(){
    	return view('admin.dashboard');
    }

    public function login(){
    	return view('login');
    }
    
    public function postlogin(Request $request){

        $attempt = Auth::attempt([
            'username' => $request->input('username'), 
            'password' => $request->input('password')
        ], $request->has('remember'));

        if($attempt){
            return redirect('/admin');
        }else{
            return back();
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }

    public function upload(Request $request){
        if($request->hasFile('file')){
            $file = $request->file('file');
            if($request->input('mime') === 'image'){
               $x = (int) $request->input('x', '0');
               $y = (int) $request->input('y', '0');
               $width = (int) $request->input('width');
               $height = (int) $request->input('height');

               //crop image and save it in uploads and database
               $cropAndSave = $this->cropAndSave($x, $y, $width, $height, $file);

               if($cropAndSave){
                // return json object of file model
                return response()->json($cropAndSave);
               }
               
            }

            if($request->input('mime') === 'audio'){
                // process audio files
                $filename = $this->generateUniqueTitle($file->getClientOriginalName());
                $file->move('uploads/audio/', $filename);

                // add file in the database
                $file = File::create([
                    'url' => "uploads/audio/".$filename,
                    'filename' => $filename,
                    'status' => true
                ]);

                return $file;
            }

            if($request->input('mime') === 'video'){
                // process video files
            }
            return ["status" => false];
        }
    }

    public function generateUniqueTitle($filename){
        // generate timestamp
        $now = new Carbon();
        $time = $now->timestamp;
        $filename = "file__".$time."_".uniqid().$this->getExtension($filename);
        return $filename;
    }

    public function getExtension($filename){
        $filename = explode(".", $filename);
        return ".".end($filename);
    }

    public function cropAndSave($x, $y, $width, $height, $file){
        $filename = $this->generateUniqueTitle($file->getClientOriginalName());
        
        // crop and save
        Image::configure(array('driver' => 'imagick'));
        $image = Image::make($file);
        $image->crop($width, $height, $x, $y);
        $image->save(public_path()."/uploads/images/".$filename, 100);


        // add file in the database
        $file = File::create([
            'url' => "uploads/images/".$filename,
            'filename' => $filename,
            'status' => true
        ]);

        return $file;
    }

}
