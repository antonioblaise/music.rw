@extends('layouts.master')

<!-- title -->
@section('title', 'Login')

@section('container')
	<div class="row">
		<div class="login-page">
			<h2 class="text-center">Login</h2>
			<div class="well col-md-4 col-md-offset-4">
				<!-- form fields -->
				{!! BootForm::open([]) !!}
					{!! BootForm::text('username', 'Username')!!}
					{!! BootForm::password('password','Password')!!}
					{!! BootForm::checkbox('remember', 'Remember me ?', null, 0) !!}

					{!! BootForm::submit('Login', ['class'=>'btn btn-primary']) !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
@endsection


@section('footer')
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<p class="align-center">&copy; {{ date('Y') }} Allright reserved</p>
	</div>
</div>
@endsection