<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>@yield('title', 'Music App')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ url('css/uploadzone/uploadzone.css') }}">
        <link rel="stylesheet" href="{{ url('css/cropper.min.css') }}">
        <link rel="stylesheet" href="{{ url('css/app.css') }}">
    </head>
    <body>
		<header>
			@yield('header')
		</header>
		
		<section id="main">
			<div class="container">
				@yield('container')
			</div>
		</section>
		
		<footer>
			@yield('footer')
		</footer>



        <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
		<script src="{{ url('js/cropper.min.js') }}"></script>		
		<script src="{{ url('js/uploadzone/uploadzone.js') }}"></script>		
		<script src="{{ url('js/app.js') }}"></script>		
    </body>
</html>