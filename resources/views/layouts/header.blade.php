<nav class="navbar navbar-default">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
		        <div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="{{ url('/admin')}}">Music App</a>
		        </div>
		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		            <ul class="nav navbar-nav">
		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Artists <span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                        <li><a href="{{ url('admin/artist/create') }}">Add Artist</a></li>
		                        <li><a href="{{ url('admin/artist/') }}">All Artists</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('admin/artist/?offline=1') }}">Offline Artists</a></li>
		                    </ul>
		                </li>

		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tracks <span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                        <li><a href="{{ url('admin/track/create') }}">Add Track</a></li>
		                        <li><a href="{{ url('admin/track/') }}">All Tracks</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('admin/bulk/track/add') }}">Add Bulk Tracks</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('admin/track/?offline=1') }}">Offline Tracks</a></li>
		                    </ul>
		                </li>

		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Videos <span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                        <li><a href="{{ url('admin/video/create') }}">Add Video</a></li>
		                        <li><a href="{{ url('admin/video/') }}">All Videos</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('admin/video/?offline=1') }}">Offline Videos</a></li>
		                    </ul>
		                </li>

		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Playlists <span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                        <li><a href="{{ url('admin/playlist/create') }}">Add Playlist</a></li>
		                        <li><a href="{{ url('admin/playlist') }}">All Playlists</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('admin/playlist/?offline=1') }}">Offline Playlists</a></li>
		                    </ul>
		                </li>

		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users <span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                        <li><a href="{{ url('admin/user/create') }}">Add User</a></li>
		                        <li><a href="{{ url('admin/user/') }}">All Users</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('admin/user/?offline=1') }}">Offline Users</a></li>
		                    </ul>
		                </li>
		            </ul>
		            <!-- 
		            <form class="navbar-form navbar-left">
		                <div class="form-group">
		                    <input type="text" class="form-control" placeholder="Search">
		                </div>
		                <button type="submit" class="btn btn-default">Submit</button>
		            </form>
		            -->
		            <ul class="nav navbar-nav navbar-right">
		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		                    My Account
		                    <span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                        <li><a href="{{ url('admin/account')}}">Account</a></li>
		                        <li><a href="{{ url('admin/settings') }}">Settings</a></li>
		                        <li role="separator" class="divider"></li>
		                        <li><a href="{{ url('/logout') }}">Logout</a></li>
		                    </ul>
		                </li>
		            </ul>
		        </div>
        	</div>
        </div>
    </div>
</nav>