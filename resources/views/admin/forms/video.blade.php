@extends('admin.master')

@section('title')
	@if($video != null)
		Edit a Video | Admin
	@else
		Create a Video | Admin
	@endif
@endsection

@section('container')
	<div class="row">
		<div class="col-md-12">
			@if($video == null)
				<h3>Add Video</h3>
			@else
				<h3>Edit Video</h3>
			@endif
			<hr>
		</div>
		{!! BootForm::open(['model' => $video, 'store' => 'admin.video.store', 'update' => 'admin.video.update']) !!}
		<div class="col-md-8">
			{!! BootForm::text('title', 'Song Title') !!}
			{!! BootForm::text('youtube_id', 'Youtube ID or URL') !!}
			{!! BootForm::select('artist', 'Artist\'s name', $artists) !!}
			{!! BootForm::submit('Save', ['class' => 'btn btn-primary']) !!}
		</div>
		<div class="col-md-4">
			{!! BootForm::select('status', 'Status', ['0' => 'Offline', '1' => 'Published']) !!}
		</div>
		{!! BootForm::close() !!}
	</div>
@endsection