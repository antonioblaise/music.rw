@extends('admin.master')
@section('title')
	@if($artist == null)
		Add Artist
	@else
		Edit Artist
	@endif
@endsection

@section('container')
	<div class="row">
		<div class="col-md-12">
			@if($artist == null)
				<h3>Add Artist</h3>
			@else
				<h3>Edit Artist</h3>
			@endif
			<hr>
		</div>
		{!! BootForm::open(['model' => $artist, 'store' => 'admin.artist.store', 'update' => 'admin.artist.update']);!!}
			<div class="col-md-8">
				{!! BootForm::text('name', 'Artist\'s Name') !!}
				{!! BootForm::textarea('biography', 'Biography') !!}
				{!! BootForm::submit('Save', ['class' => 'btn btn-primary']) !!}
			</div>
			<div class="col-md-4">
				{!! BootForm::select('status', 'Status', ['0' => 'Offline', '1' => 'Published']) !!}
				{!! BootForm::label(null, "Profile Image") !!}
				{!! BootForm::hidden('profile_image', null,['class' => 'uploadzone', 'uploadzone-accept' => 'image']) !!}
				<!-- show artist image -->
				@if($artist != null)
					<div class="form-group">
						<img src="{{ url($artist->image->url) }}" alt="{{ $artist->name }}" class="img-thumbnail img-responsive" id="profileImage">
						
					</div>
				@endif
			</div>
		{!! BootForm::close() !!}
	</div>

@endsection