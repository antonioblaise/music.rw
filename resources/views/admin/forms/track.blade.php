@extends('admin.master')

@section('container')
	<div class="row">
		<div class="col-md-12">
			@if($track == null)
				<h3>Add a track</h3>
			@else
				<h3>Edit a track</h3>
			@endif
			<hr>
		</div>
		
		{!! BootForm::open(['model' => $track, 'store' => 'admin.track.store', 'update' => 'admin.track.update']);!!}
		
			<div class="col-md-8">

					{!! BootForm::text('title', 'Song Title') !!}
					{!! BootForm::select('artist', 'Artist\'s name', $artists)!!}
					{!! BootForm::submit('Save', ['class' => 'btn btn-primary']) !!}
			</div>
			<div class="col-md-4">
				{!! BootForm::select('status', 'Status', ['0' => 'Offline', '1' => 'Published']) !!}				
				{!! BootForm::label(null, 'Select Audio file') !!}
				{!! BootForm::hidden('file', null, ['class' => 'uploadzone', 'uploadzone-accept' => 'audio']) !!}
				@if($track != null)
				<div class="form-group">
					<h4>Preview Audio</h4>
					<audio src="{{ url($track->audio->url) }}" controls=""></audio>
				</div>
				@endif
			</div>
		{!! BootForm::close() !!}
	</div>

@endsection