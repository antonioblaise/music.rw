@extends('admin.master')

@section('container')
	<div class="row">
		<div class="col-md-12">
			<h2>All Artists</h2>
			<hr>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Title</th>
						<th>Artist</th>
						<th>Created at</th>
						<th>Updated at</th>
						<th>Edit information</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tracks as $track)
					<tr>
						<td>{{ $track->title }}</td>
						<td>{{ $track->artists->name }}</td>
						<td>{{ $track->created_at }}</td>
						<td>{{ $track->updated_at }}</td>
						<td>
							{!! Form::open(["method" => "delete", "route" => ["admin.track.destroy", $track->id]]) !!}
								<a href="{{url('admin/track/'.$track->id.'/edit')}}" class="btn btn-success btn-xs">Edit</a>
								<button type="submit" class="btn btn-danger btn-xs">Delete</button>
							{!! Form::close() !!}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection