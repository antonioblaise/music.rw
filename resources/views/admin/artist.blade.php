@extends('admin.master')

@section('title')
All Artists | Admin
@endsection
@section('container')
	<div class="row">
		@foreach($artists as $artist)
			<div class="col-md-3 col-sm-12">
				<div class="thumbnail">
					<img src="{{ url($artist->image->url) }}" alt="{{ $artist->name }}">
					<div class="caption">
						<h3><a href="{{ url('admin/artist/'.$artist->id.'/edit') }}">{{ $artist->name}}</a></h3>
						{!! Form::open(["method" => "delete", "route" => ["admin.artist.destroy", $artist->id]]) !!}
							<a href="{{ url('admin/artist/'.$artist->id.'/edit') }}" class="btn btn-info">Edit Artist</a>
							<button type="submit" class="btn btn-danger">Delete</button>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		@endforeach
		<div class="col-md-12">
			{{ $artists->links() }}
		</div>
	</div>

@endsection