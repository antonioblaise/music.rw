/*
	Uploadzone jQuery plugin
	Developed by IRADUKUNDA Blaise Antonio
	Version 1.0.0
*/

( function( $ ){
	$.fn.uploadzone = function(options){
		var $this = this;

		var settings = $.extend({
			url: "/upload",
			formData: {}
		}, options);

		var parentClicked = null;

		$(this).after("<input type='file' class='hide' id='fileselector' accept='audio/*|video/*|image/*'>");
		$(this).trigger("uploadzone.started");
		
		return $this.each(function(){
			var input = this;
			
			$(this).wrap("<div class='form-group uploadzone-elem'></div>");
			$(this).after("<div class='uploadzone-filename'></div>");
			$(this).after("<button class='btn btn-success'>Choose a file</button><br>");

			$(".uploadzone-elem button").off("click").click(function(event){
				event.preventDefault();
				parentClicked = $(this).parent();
				openDialog();				
			});


			function toggleDialog(){
				$("body").toggleClass("uploadzone-dialog-open");
			}

			function openDialog(){
				var dialogTemplate = [
					'<div class="uploadzone-dialog uploadzone-responsive-panel">',
						'<div class="uploadzone-dialog-inner-wrap">',
							'<div class="uploadzone-dialog-close">x</div>',
							'<div class="uploadzone-dialog-panel">',
								'<div class="uploadzone-dialog-panel-tabs current uploadzone-dragdrop">',
									'<div class="uploadzone-dialog-file-title"><h3>Drop a file here</h3></div>',
									'<div class="uploadzone-dialog-file-title"><span>or</span></div>',
									'<div class="uploadcare-dialog-file-click">',
										'<button class="btn btn-info btn-lg">Choose a local file</button>',
									'</div>',
								'</div>',
								'<div class="uploadzone-dialog-panel-tabs uploadzone-preview">',
								'</div>',
							'</div>',
						'</div>',
					'</div>'
				].join("");

				toggleDialog();

				$("body").append(dialogTemplate);
				$($this).trigger("uploadzone.dialog.opened");

				$(".uploadzone-dialog").click(function(event){
					closeDialog(event);
				});

				$(".uploadzone-dragdrop button").click(function(event){
					$("#fileselector").click();
				});

				// file drag over events
				$(".uploadzone-dragdrop").bind({
					dragover: function(e){
						e.preventDefault();
						$(this).addClass("dragover");
					},
					dragenter: function(e){
						e.preventDefault();
					},
					dragleave: function(e){
						$(this).removeClass("dragover");
					}
				});

				var drop = document.getElementsByClassName("uploadzone-dragdrop")[0];

				drop.addEventListener("drop", function(e){
					e.preventDefault();
					$(this).removeClass("dragover");
					processFiles(e.dataTransfer.files);
				}, false);

				$("#fileselector").change(function(e){
					processFiles(e.currentTarget.files);
				});


			}

			function closeDialog(event){
				var fileselector = $("#fileselector");
				var target = $(event.target);
				if(target.hasClass("uploadzone-dialog") || target.hasClass("uploadzone-dialog-close") || event == "finish"){
					$(".uploadzone-dialog").remove();
					$($this).trigger("uploadzone.file.added");
					fileselector.replaceWith(fileselector.val('').clone(true));
					toggleDialog();
				}
			}

			function processFiles(files){
				$($this).trigger("uploadzone.drop.done", [files]);
				var file = files[0];
				var fileType = getMediaType(file);
				

				if(fileType == "image" && isAccepted(fileType)){
					previewImage(file)
				}

				if(fileType == "audio" && isAccepted(fileType)){
					previewAudio(file);
				}

				if(fileType == "video" && isAccepted(fileType)){
					previewVideo(file);
				}
			}

			function isAccepted(type){
				var acceptAttribute = $(input).attr("uploadzone-accept");
				if(acceptAttribute){
					var accepted = acceptAttribute.split(" ");
					if(accepted.indexOf(type) >= 0){
						return true;
					}
				}
				return false;
			}

			function previewImage(file){
				var image = file;
				var preview = $(".uploadzone-preview");
				var previewTemplate = [
					'<div class="uploadzone-preview-inner-wrap">',
						'<h3>Add and Crop Image</h3>',
						'<div class="uploadzone-preview-image-wrap">',
							'<img class="uploadzone-preview-image hide">',
						'</div>',
						'<div class="uploadzone-preview-action">',
							'<button class="btn btn-success uploadzone-action-upload">Upload</button>',
							'<button class="btn btn-danger uploadzone-action-cancel">Cancel</button>',
						'</div>',
					'</div>'
				].join("");


				if(preview.has(".uploadzone-preview-inner-wrap").length == 0){
					$(".current").removeClass("current");
					preview.addClass("current");
					preview.append(previewTemplate);

					// read file 
					var reader = new FileReader();
					reader.readAsDataURL(file);

					reader.onloadend = function(e){
						var image = $(".uploadzone-preview-image");
						image.attr("src", e.currentTarget.result);
						image.removeClass("hide");
						
						if($.fn.cropper != undefined){
							image.cropper({
								aspectRatio: 1,
								viewMode: 1,
								zoomable: false,
								zoomOnTouch: false,
								zoomOnWheel: false,
								autoCropArea: 0.4
							});
						}
					}
					$(".uploadzone-action-cancel").click(function(){
						preview.empty();
						preview.removeClass("current");
						$(".uploadzone-dragdrop").addClass("current");
						var fileselector = $("#fileselector");
						fileselector.replaceWith(fileselector.val('').clone(true));
					});

					$(".uploadzone-action-upload").click(function(){
						var cropData = $(".uploadzone-preview-image").cropper("getData", true);
						var data = {
							size: file.size,
							mime: getMediaType(file),
							x: cropData.x,
							y: cropData.y,
							width: cropData.width,
							height: cropData.height,
						};

						upload(data, file);

					});
				}


			}

			function previewAudio(file){
				var audio = file;
				var preview = $(".uploadzone-preview");
				var previewTemplate = [
					'<div class="uploadzone-preview-inner-wrap">',
						'<h3>Add This Audio</h3>',
						'<div class="uploadzone-preview-audio-wrap">',
							'<audio class="uploadzone-preview-audio" controls></audio>',
						'</div>',
						'<div class="uploadzone-preview-action">',
							'<button class="btn btn-success uploadzone-action-upload">Upload</button>',
							'<button class="btn btn-danger uploadzone-action-cancel">Cancel</button>',
						'</div>',
					'</div>'
				].join("");
				
				if(preview.has(".uploadzone-preview-inner-wrap").length == 0){
					$(".current").removeClass("current");
					preview.addClass("current");
					preview.append(previewTemplate);

					var audioPlayer = $(".uploadzone-preview-audio");

					var reader = new FileReader();
					reader.readAsDataURL(file);
				
					reader.onloadend = function(e){
						var url = e.currentTarget.result;
						audioPlayer.attr("src", url);
					}

					$(".uploadzone-action-cancel").click(function(){
						preview.empty();
						preview.removeClass("current");
						$(".uploadzone-dragdrop").addClass("current");
						var fileselector = $("#fileselector");
						fileselector.replaceWith(fileselector.val('').clone(true));
					});

					$(".uploadzone-action-upload").click(function(){
						var data = {
							size: file.size,
							mime: getMediaType(file),
						};
						upload(data, audio);
					});
				}
				
			}

			function previewVideo(file){
				var reader = new FileReader();
				reader.readAsDataURL(file);

				var preview = $(".uploadzone-preview");
				var previewTemplate = [
					'<div class="uploadzone-preview-inner-wrap">',

					'</div>'
				];
				
				$(".current").removeClass("current");
				preview.addClass("current");
				preview.append(previewTemplate);
			}

			function getMediaType(file){
				var mime = file.type;
				var imageMime = ["image/jpeg", "image/png", "image/gif", "image/bmp"];
				var audioMime = ["audio/mpeg3", "audio/x-mpeg-3", "audio/wav", "audio/x-wav", "audio/mp3"];
				var videoMime = ["video/mp4", "video/mpeg", "video/webm"];

				if(imageMime.indexOf(mime)>= 0){
					return "image";
				}

				if(audioMime.indexOf(mime)>= 0){
					return "audio";
				}

				if(videoMime.indexOf(mime)>= 0){
					return "video";
				}
			}

			function upload(data, file){
				// send ajax request
				var formData = new FormData();
				var items = settings.formData;

				// if you use wants to add extra form data to send via ajax
				for(var key in items){
					formData.append(key, items[key]);
				}

				for(var key in data){
					formData.append(key, data[key]);
				}

				// add file to our form data
				formData.append("file", file);

				$.ajax({
					url: settings.url,
					type: 'POST',
					data: formData,
					cache: false,
					dataType: 'json',
			        processData: false,
			        contentType: false,
			        beforeSend: function(data){
			        	$($this).trigger("uploadzone.upload.beforeSend", [input]);
			        },
			        success: function(data, textStatus, jqXHR){
			        	console.log(data);

			        	$($this).trigger("uploadzone.upload.done", [input, data]);
			        	$(".uploadzone-filename").text(data.filename);
			        	closeDialog("finish");
			        },
			        error: function(jqXHR, textStatus, errorThrown){
			        	$($this).trigger("uploadzone.upload.error", [textStatus, errorThrown]);
			        },
				});
			}



		});
	};

}( jQuery ));


