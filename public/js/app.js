$(document).ready(function(){
	
	if($("body").has("#biography").length > 0){
		CKEDITOR.replace('biography');
	}

	var uploadzone = $(".uploadzone").uploadzone({
		formData: {
			_token : $("input[name='_token']").val()
		}
	});

	uploadzone.on("uploadzone.uploadzone.beforeSend", function(event, input){
		// check if content has a value //
	});

	uploadzone.on("uploadzone.upload.done", function(event, input, data){
		$(input).val(data.id);

		if($("body").has("#profileImage").length > 0){
			$("#profileImage").attr("src", "/"+data.url);
		}
	});


	$(".delete-btn").click(function(){
		
	});
});